#!/usr/bin/perl

use strict;
use File::Find::Rule;
use File::Basename;
use File::Spec;

my $dirname = '/scratch/malay/TCGA_RNASeqV2/';				# get directory name from command line

opendir(DIR, $dirname) || die "Can't open $dirname\n";				# open directory 

while (my $dir = readdir(DIR)){
	next if ($dir =~ m/^\./);
	my $cancer_name = $dir;
	my $cancer = File::Spec->catdir($dirname, $dir);				# directory of all cancer types

	my @sdrf_file = File::Find::Rule->file->name('*.sdrf*')->in($cancer);				# searching for all sdrf files

	my %id_file_map;

	foreach my $sdrf_file (@sdrf_file){
		open (my $fh, "bunzip2 -c $sdrf_file |") || die "Can't open $sdrf_file";

		while (my $line = <$fh>){
			next if $line =~ /^\#/;
			my @columns = split("\t", $line);
			$id_file_map{@columns[21]} = @columns[1];
		}
	}

	my @normalised_gene_exp = File::Find::Rule->file->name('*genes.normalized*')->in($cancer);

	foreach my $normalised_gene_exp (@normalised_gene_exp){
		my $basename = basename($normalised_gene_exp, ".bz2");
		die "TCGA id not found" unless exists $id_file_map{$basename};
		my $tcga_id = $id_file_map{$basename};
		
		open (my $ge, "bunzip2 -c $normalised_gene_exp |") || die "Can't open $normalised_gene_exp";
		
		while (my $l = <$ge>){
			next if $l =~ /^\#/;
			chomp $l;
			#next if ($l eq "gene_id normalized_count");

			my ($gene, $expression) = split("\t", $l);
			next if ($gene eq "gene_id");

			print join("\t", $cancer_name, $tcga_id, $gene, $expression, $basename), "\n";
		}
	}
}